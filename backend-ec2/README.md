
# CS 373 Backend Deployment Tutorial - Flask on EC2

Canyon Mooney

This tutorial documents the actual process I took to create a Flask app hosted on EC2. I start with a simple set up and layer on complexity as I go. I hope this approach will make the deployment process feel more intuitive and natural.

### Table of Contents
<pre>
   I. Creating a Flask App
      1. Flask Routing
      2. Python Venv

  II. Creating an EC2 Instance
      1. Adventures in AWS 1/
      2. Initial Contact
      3. Setting Up Docker

 III. Piercing the Onion
      1. Production Deployment
      2. Let Me In!!!
      3. How Do We Know it Worked? (AiA 2/)

  IV. The HTTPS Conondrum
      1. Praise the Elastic Load Balancer (AiA 3/)
      2. Curse the Elastic Load Balancer
      3. Routing External Connections (AiA 4/)
      4. A Last Fix (AiA 5/)

   V. That Which is Left Undone
</pre>

## I. Creating a Flask App

### __I-1. Flask Routing__

#### Code

First I set up some basic routes in `endpoints.py` using Flask.

These two lines import Flask and create a new Flask app, stored in `flaskApp`.

```python
from flask import Flask

flaskApp = Flask(__name__)
```

Next I added some basic routes.

```python
@flaskApp.route("/")
def home():
    return "Hello I Am Here!"

@flaskApp.route("/whois/<name>")
def whois(name):
    return "Hello, " + name + ", that is your name!"
```

Finally, I added this code to detect when `endpoints.py` is run directly from the command line. If this is the case, it will run the Flask app on `localhost:5000` using the Flask development server.

```python
if __name__ == "__main__":
    flaskApp.run(port=5000, debug=True);
```

#### Testing

To test my code I ran `python endpoints.py` inside the target directory.

- Note that I was able to do this because I have previously installed Flask in my global env (not recommended). The next section explains how to set up a virtual env (venv).

- Also instead of `python` you may need to directly invoke `python3` on your machine, depending on how its configured.

<img src="./img/run-flask-1.png" />


Confirmed I can access the endpoints on localhost.

<img src="./img/flask-output-1.png" height="200px" />
<img src="./img/flask-output-2.png" height="200px" />

### __I-2. Setting up a Python Venv__

#### Creation

I created a venv to store my dependencies. Venv is a module that comes with python so I ran `python -m venv path/to/venv`, where the last variable is the directory the venv will live.

<img src="./img/venv-1.png" />

#### Activation

It took me a hot second to figure out how to activate the venv. Since I was using bash, I ran `source ./example-venv/Scripts/activate` to activate the venv. You can see below that there are at least three different scripts to activate a venv. The one you use will depend on the shell you're using. `Activate.ps1` works with Powershell and `activate.bat` works with the default Windows cmd. 

<img src="./img/venv-actv.png" height="300px"/>

#### Installation

After installing Flask and its dependencies with `pip install flask`, I used `pip freeze > requirements.txt` to dump everything I needed into `requirements.txt`. My plan was to use the requirements file when deploying to the EC2 instance.

<img src="./img/venv-2.png" height="500px"/>

#### Git Ignore <3

Finally, I added the venv to my gitignore file.

<img src="./img/gitignore-venv.png" height="75px" />


## II. Creating an EC2 Instance

### __II-1. Adventures in AWS 1/__

Do you like AWS? 

No???? Me neither! 

For this part of the tutorial, I thought I would basically take a screenshot every time I did something that changed the view area in AWS. I have the burning desire that an Amazon exec will see this tutorial and thereby come to understand why we despise AWS (despite it being legitimately very useful).

<b>Click on the images if you need to see them full size.</b>

#### STEP 1

This is the home page right after I logged into AWS. I clicked on "EC2".

<img src="./img/aws-1.png" height="300px"/>

#### STEP 2

From the EC2 dashboard, I clicked on the yellow "Launch instance" button near the bottom.

<img src="./img/aws-2.png" height="300px"/>

#### STEP 3

I named my instance "cs373-backend".

<img src="./img/aws-3.png" height="300px"/>

#### STEP 4 - Image Instance

For the image instance, I choose Ubuntu Server 22.04 LTS and x86-64 architecture. EC2 is basically a rent-a-computer in the cloud, and this is the OS for that computer. There's no particular reason why I made these choices beyond an inherent trust of Ubuntu and an inherent distrust of the Amazon brand. You could choose Amazon Linux or any other free tier elegible image and I'm sure it would be fine.

<img src="./img/aws-4.png" height="300px"/>

#### STEP 5 - Instance Type

I didn't change the default instance type. Don't feel discouraged that Amazon's free tier only grants 1 vCPU and 1 GB of memory. The t2 class of instances accrue credits for unused resources, then automatically spend those credits to burst beyond the default limits. It was more than enough for me to run my setup without performance issues.

<img src="./img/aws-5.png" height="300px"/>

#### STEP 6a - Creating a Key Pair

<b>THIS PART IS VERY VERY IMPORTANT!!!</b> After creating a new key, do not lose it!

This private key is the <b>one and only</b> way to gain initial access the EC2 instance. If you lose this key, you will need to make a new EC2 instance.

- Once you have access to the instance, you can use [this documentation](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/managing-users.html) to add your team mates as users.

<img src="./img/aws-6.png" height="300px"/>

#### STEP 6b - Saving Your Private Key

Immediatly after downloading, I put my private key in my `.ssh` folder.

<img src="./img/aws-7-key.png" height="250px"/>

#### STEP 7 - Security Group

By default, the wizard creates a new security group. This is fine. The security group controls what kind of traffic (think protocol, application, ports, source, etc.) are allowed to make contact with the EC2 instance. It's like a reverse restraing order. By default if a connection isn't marked as allowed in the security group, it is rejected.

I edited my security group to allow me to connect using SSH from my IP address. This is how I will access the EC2 instance. Also, I trust everyone in this class to use their CS powers for good only, and never evil. That is, please don't ddos me now that I've exposed my IP.

We will modify this security group later when we link the instance to the wider world.

<img src="./img/aws-8.png" height="300px"/>

#### STEP 8 - Configure Storage

I didn't touch the default of 8 GB. I was skeptical it wouldn't be enough but it ended up being fine. Under the free tier, you can use up to 30 GB of elastic block store (EBS) storage.

- Note that you can't change how much storage an EC2 instance has after you create it.

<img src="./img/aws-9.png" height="300px"/>

#### STEP 9 - Feel Satisfaction

I found the "Connect to Instance" link near the bottom to be a helpful read, but you don't need to click on it.

<img src="./img/aws-10.png" height="300px"/>


### __II-2. Initial Contact__

Okay cool, I made an instance. It's like making a kid. Now it's time to feed the kid (connect to the instance). They have all these orifices (IP and pseudo-IP things) and you need to figure out which one to put food in (connect to). And if you pick the wrong one, it's a vist to the ER to removed mashed peas from the inner ear. Fortunatly, the consequences are not so severe in the case of instances.

Basically look for things marked with the word <b>public.</b> 

<img src="./img/ec2-0.png" height="400px"/>

#### SSH

First I navigated to my `.ssh` folder so I could use the key I saved when I set up the instance.

I was able to SSH into my instance using `ssh -i ./name-of-my-key.pem ubuntu@ec2-3-12-150-1.us-east-2.compute.amazonaws.com`. 

The path following `-i` is the identity to use when connecting. 

`ubuntu` is the default username of the EC2 instance, but it might be different depending on the image you chose (`ec2-user` is common). See a list of common default usernames [here](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/managing-users.html).

`ec2-3-12-150-1.us-east-2.compute.amazonaws.com` is the public IPv4 DNS. All that means is this URL resolves to the public IP address of the instance. 

<img src="./img/ec2-1.png" height="300px"/>

Just to show that you can use the public IP address of the instance directly.

<img src="./img/ec2-2.png" height="300px"/>

For some reason, the first thing I did upon SSHing into my instance was try to install tmux (terminal multiplexer), which basically lets you detach process started in the terminal so they can run on their own. It turns out it was already installed and I haven't used it quite yet.

<img src="./img/ec2-3.png" height="300px"/>

### __II-3. Setting Up Docker__

#### Installation

To install Docker, I pretty much copy and pased code from the docker install instructions [here](https://docs.docker.com/engine/install/ubuntu/#installation-methods) (note this is for Ubuntu specificially). The copy-paste method actually worked flawlessly. Just make sure to do all three parts in order.

#### My Dockerfile

I added four lines to my Dockerfile. My base image is just `python:latest`. Then I get the requiremnts file I was crowing about earlier and installed it using pip.

The last line starts bash when I run the Docker image. I did this for flexibility and will probably change it to a more targeted deploy command later.

```Docker
FROM python:latest

COPY ./requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

CMD ["/bin/bash"]
```

#### Docker Build

<b>I cloned my repo into the instance.<b/>

Then I navigated to my backend directory.

To build the Docker image, I used `sudo docker build --tag backend --file ./Dockerfile .` (note period at the end). It took a couple minutes to build. It's quicker in the screenshot because Docker probably had the `python:latest` image cached from a previous failed build.

`--tag backend` is basically the name of the docker image. `--file ./Dockerfile` is the source file. `.` is the output directory I believe.

#### Docker Run

To run the Docker image, I used `sudo docker run --rm -i -t -v /home/ubuntu/cs373-public-ta/backend-ec2:/usr/python backend`. I got this command from the [lecture](https://www.cs.utexas.edu/users/downing/cs373/Schedule.html) on Dockerfiles.

<img src="./img/docker-1.png" height="400px"/>

#### Starting Flask

I confirmed Docker was working correctly by navigating to `/usr/python` and starting my flask app with `python endpoints.py`.

<img src="./img/docker-2.png" height="400px"/>


## III. Piercing the Onion

### __III-1. Production Deployment__

#### Robust Servers

A server is a remarkable piece of software. You start it and it basically runs continuously after that. A server may go months without a reboot. The reason being that it's being used continuously, so a reboot would cause a not insignificant disruption. Considering that the solution to 99% of software problems is to reboot the damn thing, production servers must be really, really robust.

The Flask development server is not. 

However, Flask maintains a list of compatible production servers [here](https://flask.palletsprojects.com/en/3.0.x/deploying/). We will run our Flask app using one of these. I arbitrarily chose gunicorn because it sounds like unicorn.

I tested that this worked on my EC2 instance by running `pip install gunicorn` and quickly checking it would run my WSGI (Flask) app. Initally I ran `gunicorn -w 2 'endpoints:flaskApp'`.

<img src="./img/gunicorn-1.png" height="300px"/>

#### Gunicorn Config

I played around with the settings a little bit and decided that `gunicorn -w 2 -b 0.0.0.0:8080 'endpoints:flaskApp'` would work for my purposes.

`-w 2` is the number of worker processes the server uses. `-b 0.0.0.0:8080` is the IP and port the server listens on. I chose port `8080` arbitrairly and `0.0.0.0` will map to any and all IPs associated with the host I believe. `'endpoints:flaskApp'` represents the Python file in the source directory and the variable associated with the Flask app.

Also I ran gunicorn as root (a security issue). You can avoid this by adding a [USER](https://docs.docker.com/engine/reference/builder/#user) directive to your Dockerfile.

<img src="./img/gunicorn-2.png" height="350px"/>

#### Add to Requirements File

I added gunicorn to my `requirements.txt` file so the next time I built my Docker image it would be installed by default.

<img src="./img/gunicorn-3.png" height="150px"/>

### __III-2. Let Me In!!!__

At this point, I addressed the issue of networking between Docker, the EC2 instance, and the wider world.

#### Testing Using Curl

First I started the server inside the Docker runtime.

<img src="./img/gunicorn-3.25.png" height="150px"/>

Then in another terminal window, I ran `sudo docker exec -it <container id> bash -c 'curl http://localhost:8080'`. The reply told me the server was accessable from within Docker. However, running `curl http://localhost:8080` directly gave an error. And so we cannot yet access the server from outside the container.

<img src="./img/gunicorn-3.5.png" height="400px"/>

#### Using --network=host

I solved this issue by starting my Docker container with the `--network=host` flag, which lets the Docker container use the network stack of the EC2 instance. Read more [here](https://docs.docker.com/network/drivers/host/).

- Note that this option ONLY works in Linux environments.

- Fortunatley, the same effect can be achived using [port-mapping](https://docs.docker.com/network/) and the [EXPOSE](https://docs.docker.com/engine/reference/builder/#expose) directive.

I don't know that one way is better than another. If anything, I just wanted to show an alternate way to let connections into the Docker container.

<img src="./img/gunicorn-4.png" height="200px"/>

Now when I run the `curl` commands, I can access the sever both inside and outside the Docker container.

<img src="./img/gunicorn-5.png" height="150px"/>

### __III-3. How Do We Know it Worked?__
### __(III-3. Adventures In AWS 2/)__

And to really make sure I would be able to access the server from outside Docker, I modified the EC2 security group to allow a test ping from my local machine.

Once again, I followed the philosophy of screenshot everything in AWS.

#### STEP 1

I started from the instance summary page.

<img src="./img/test-ping-1.png" height="300px"/>

#### STEP 2

And scrolled down to inbound and outbound rules. Then I clicked to open the security group "launch-wizard-2" in a new tab.

<img src="./img/test-ping-2.png" height="300px"/>

#### STEP 3

I clicked on the "Inbound rules" tab.

<img src="./img/test-ping-3.png" height="300px"/>

#### STEP 4 - Edit Inbound Rules

<img src="./img/test-ping-4.png" height="300px"/>

#### STEP 5 - Add Rule

I added a rule allowing traffic to connect from my IP on port 8080.

<img src="./img/test-ping-5.png" height="300px"/>

#### STEP 6 - Confirm Rule

I confirmed the new inbound rule appeared when I refreshed the instance summary page.

<img src="./img/test-ping-6.png" height="300px"/>

#### STEP 7 - Confirm Connection

I confirmed I can connect directly to `3.12.150.1:8080`.

<img src="./img/test-ping-7.png" height="100px"/>

#### STEP 8 - What about HTTPS?

There's still more work to be done.

<img src="./img/test-ping-8.png" height="300px"/>

## IV. The HTTPS Conondrum

#### WHY.

So how come we can't use HTTPS to connect to our instance?

This is actually an important conceptual question for two reasons. First it gets at the heart of how the application layer of the network is built on top of the protocol layer. Second it shows that different network application standards don't necessarily mesh on one server. 

#### Protocol Layer vs Application Layer

Both the HTTP and HTTPS standard use TCP at the protcol level to work. So our rule allowing TCP connections on port 8080 should let either through. And indeed, instead of a connection refused error, we get an error about secure connection failing.

- Side note: It is important that the port is specificed in the URL because by default HTTP connects using port 80 on TCP and HTTPS connects using port 443 on TCP, neither of which we allow in the security group.

#### Supporting HTTPS

The secure connection failed for a really basic reason. Our server (Gunicorn) doesn't support HTTP<b>S</b>! If you search the [Flask deploy docs](https://flask.palletsprojects.com/en/3.0.x/deploying/), you'll notice that Gunicorn et al. are actually all [WSGI servers](https://wsgi.readthedocs.io/en/latest/what.html) which naturally bridge between the HTTP (no S) standard and the WSGI standard. 

Searching around some more, you might find some information regarding SSL support on [this page](https://flask.palletsprojects.com/en/3.0.x/deploying/uwsgi/) for uWSGI.

It mentions using a reverse proxy (e.g., nginx); that is, a secondary server to receive incoming connections and pass them on to the WSGI server using HTTP. And it also mentions a vague notion of built-in SSL support. 

We will adopt a method similar to the reverse proxy method, but we can't use our own server to do it. This is becuase we lack the essential element of an HTTPS server, an SSL certificate. EC2 is a rent-a-computer in the sky and nothing else. If we want a certificate to use on it, we need to get one and put it there.

But let's not. Getting a certificate is hard and probably cost money and I make $14 bucks an hour (as a high-skill TA!!?!?).

### __IV-1. Praise the Elastic Load Balancer__
### __(IV-1. Adventures in AWS 3/)__

Instead, we will use a nifty piece of AWS middleware to act as our reverse proxy server, an Elastic Load Balancer!

WARNING: This is an involved process. The tutorial for createing a load balancer involves two sub-tutorials:

- Making a Target Group (Step 7)
- Requesting an SSL Cert (Step 10)

#### STEP 1

From the EC2 dashboard, click on "Load balancers".

<img src="./img/elb-1.png" height="300px"/>

#### STEP 2

From the load balancers page, click "Create load balancer".

<img src="./img/elb-2.png" height="300px"/>

#### STEP 3 - Choose Load Balancer Type

<img src="./img/elb-3.png" height="300px"/>

I picked a network load balancer because I saw the phrase "TLS offloading" (TLS is an extension of TCP that supports HTTPS). Based on the description, it sounds like the application load balancer should work as well, but I've had problems with it in the past. 

- You might experiment and see how the two load balancers work differently.

<img src="./img/elb-4.png" height="300px"/>

#### STEP 4 - Basic Config

I named my load balancer `backend-nlb`. I made it internet-facing using IPv4.

<img src="./img/elb-5.png" height="300px"/>

#### STEP 5 - Network Mapping

I used an existing virtual private cloud (VPC), the one that contained my EC2 instance.

VPCs are how disparate AWS service communicate with each other internally, so I considered it important that my load balancer and EC2 instance shared a VPC.

I selected all availability zones.

<img src="./img/elb-6.png" height="300px"/>

#### STEP 6 - Security Group

<b>IMPORTANT NOTE:</b> I ended up needing to modify this setting later on.

I chose the default group becuase I thought it would allow all incoming and outgoing traffic, but on reflection I think security groups for load balancers work differently becuase it ended up blocking external traffic!

I would just not add any security group if I was to do this again.

<img src="./img/elb-7.png" height="300px"/>

##### SUBSECTION - TARGET GROUPS - BEGIN
#### STEP 7 - Listeners and Routing

First I opened the link to create a new target group in a new tab. The target group will contain my EC2 instance.

<img src="./img/elb-8.png" height="300px"/>

#### STEP 7-A - Target Type

I chose instances, like my EC2 instance. Note the instance must be in the same VPC as the load balancer.

<img src="./img/elb-9.png" height="300px"/>

#### STEP 7-B - Target Config

I named my target group `cs373-backend-tgt-grp`, I set it to receive traffic on port 8080 using TCP, with an eye towards my Gunicorn setup. Since this is the receiving port and protocol (not the forwarding port and protocol), I think you could change it without issue (test it!).

I made sure to pick the VPC containing my EC2 instance.

<img src="./img/elb-10.png" height="300px"/>

#### STEP 7-C - Health Checks

I left this with the default settings because I knew I had a route mapped to root `/`.

I scrolled down and clicked the yellow button to continue.

<img src="./img/elb-11.png" height="300px"/>

#### STEP 7-D - Register Targets

I chose my EC2 instance and made sure that traffic will be routed on port 8080, since this should go directly to my Gunicorn server now.

<img src="./img/elb-12.png" height="300px"/>

#### STEP 7-E - Create Target Group

After clicking "Include as pending below" my screen looked like this. I then clicked "Create target group".

<img src="./img/elb-13.png" height="300px"/>

##### SUBSECTION - TARGET GROUP - END

#### STEP 8 - TCP:80 Listener

After refreshing, I could pick the target group I had just made. I configred the listener for look for HTTP traffic on port 80 (the default). I then clicked "Add listener" for TLS traffic.

<img src="./img/elb-14.png" height="300px"/>

#### STEP 9 - TLS:443 Listener

I used the same target group, but configured this to listen to TLS traffic on port 443, which should contain HTTPS traffic by default.

<img src="./img/elb-15.png" height="300px"/>

##### SUBSECTION - SSL CERT - BEGIN
#### STEP 10 - Secure Listener Settings

I left the defaults as is and opened "Request new ACM certificate" in a new tab.

<img src="./img/elb-16.png" height="300px"/>

#### STEP 10-A - ACM Homepage

<img src="./img/elb-17.png" height="300px"/>

#### STEP 10-B - Request Certificate

<img src="./img/elb-18.png" height="300px"/>

#### STEP 10-C - Domain and Validation

<img src="./img/elb-19.png" height="300px"/>

#### STEP 10-D - Click Request

<img src="./img/elb-20.png" height="300px"/>

#### STEP 10-E - Request Submitted

From here, I clicked "View certificate".

<img src="./img/elb-21.png" height="300px"/>

#### STEP 10-F - Pending Valiation

I scrolled down to the validation CNAMES.

<img src="./img/elb-22.png" height="300px"/>

#### STEP 10-G - Validation Records

I clicked the button to create recods in Route53. If your domain isn't in Route53, you can just need to add the records manually.

- Note that you don't have the use the DNS record service of your domain registrar. My domain registrar is Namecheap. To move my domain to Route53, I beleive I just needed to update the name servers for `canmoo.me` in Namecheap's settings. 

- [Here](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/MigratingDNS.html) are the AWS docs for switching DNS services (not the same thing as a domain transfer).

<img src="./img/elb-23.png" height="300px"/>

#### STEP 10-H - Create Records in Route53

I clicked the yellow button.

<img src="./img/elb-24.png" height="300px"/>

#### STEP 10-I - Success (Almost)

All I had to do now was wait. It took like 5 minutes.

<img src="./img/elb-25.png" height="300px"/>

#### STEP 10-J - Done Waiting

I refreshed and eventually I saw the status had changed to green. Now I switched back to the load balancer window.

<img src="./img/elb-26.png" height="300px"/>

##### SUBSECTION - SSL CERT - END

#### STEP 11 - Choose Certificate

I chose the certificate I had just created. Note that at this point, I haven't actually routed any traffic from `cs373-backend.canmoo.me` anywhere. All I did was make a certificate that verifies I actually own that subdomain.

<img src="./img/elb-27.png" height="300px"/>

#### STEP 12 - Look at the Summary

Then scroll down and click the yellow button.

<img src="./img/elb-28.png" height="300px"/>

#### STEP 13 - FINALLY

Here I clicked "View load balancer" like an eager child.

<img src="./img/elb-29.png" height="300px"/>

#### STEP 14

I clicked into the detail page for my new load balancer.

<img src="./img/elb-30.png" height="300px"/>

#### STEP 15

Now all I had to do was wait for the load balancer to finish provisioning.

<img src="./img/elb-tst-1.png" height="300px"/>

### __IV-2. Curse the Elastic Load Balancer__

While the load balancer was provisioning, I started up the Gunicorn sever.

- Note I still need to configure the sever to run stand-alone. If I quit the SSH session, the Docker image will die.

#### Update Security Group

My IP had actually changed from Saturday to Sunday, so I had to update the EC2 security group to use my new IP. It was in the /32 subnet though (the last number after the dot), so I probably could have reconfigred it to accept connections from 209.166.123/24 and it would be fine.

<img src="./img/elb-tst-2.png" height="300px"/>

### SSH and Start Gunicorn

Now I could SSH into my EC2 instance.

<img src="./img/elb-tst-3.png" height="300px"/>

And I started the Gunicorn server as I've done before.

<img src="./img/elb-tst-4.png" height="300px"/>

#### Trouble Brewing

Back on the load balancer overview, I clicked into the target group.

<img src="./img/elb-tst-5.png" height="300px"/>

I noticed it was failing the health check, which meant the load balancer couldn't access the test endpoint (`/`).

<img src="./img/elb-tst-6.png" height="300px"/>

#### A Mystery

Back on the actual EC2 instance, a call to `curl localhost:8000` worked fine.

<img src="./img/elb-tst-7.png" height="300px"/>

Furthermore, I was able to access the instance on my local machine. Notably, I connected directly by IP.

<img src="./img/elb-tst-8.png" height="150px"/>

This told me that, while there was an issue causing the load balancer to fail to connect to the EC2 instance, the issue had to be outside the actual instance.

This is an inportant part of debugging, especially in a VERY complex sevice like AWS. Context matters and you can work it to extract information, even when there aren't any error logs.

I put these pieces of information together to isolate the problem as somewhere between the load balancer and the EC2 instance.

#### Update the Security Group

The only thing I could think between the load balancer and the EC2 instance was the security group, which made sense since I had limited acceptable IPs to only mine.

So I found the security group for the instance.

<img src="./img/elb-sec-1.png" height="300px"/>

Scrolled down to inbound rules, and clicked edit.

<img src="./img/elb-sec-2.png" height="300px"/>

And changed my IP to any IP.

<img src="./img/elb-sec-3.png" height="300px"/>

#### Ta-Da!!!

And so the light shone green.

<img src="./img/elb-sec-4.png" height="300px"/>

### __IV-3. Routing External Connections__
### __(Adventures in AWS 4/)

At this time, I decided to work towards directly connecting using the `cs373-backend.canmoo.me` domain.

#### STEP 1

My domain is in Route53, so I started from the Route53 dashboard. I clicked into my hosted zones.

- It will cost you like $0.50 to move your domain into Route53. I do recommend it becuase it makes the whole process a lot simpler. You should be able to attach a subdomain to a AWS load balancer in a non-AWS zone file, but you may need to really read up on record types to figure out how to do it correctly.

- [Here](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/MigratingDNS.html) are the AWS docs for switching DNS services (not the same thing as a domain transfer).

<img src="./img/route53-1.png" height="300px"/>

#### STEP 2

Then I clicked on canmoo.me.

<img src="./img/route53-2.png" height="300px"/>

#### STEP 3

I cliked on the yellow "Create record" button.

<img src="./img/route53-3.png" height="300px"/>

#### STEP 4

I created an alias record, which is conviently specific to Route53 and not part of the zone file standard. I chose Alias to Network Load Balancer and picked the one containing all my blood and sweat and tears.

Then I scrolled down and created the record.

<img src="./img/route53-4.png" height="300px"/>

#### STEP 5

This was the screen afterwards. I clicked "View status".

<img src="./img/route53-5.png" height="300px"/>

#### STEP 6

And it said "INSYNC" so I assumed it had been set up correctly.

<img src="./img/route53-6.png" height="300px"/>

### __IV-4. A Last Fix__
### __(Adventures in AWS 5/)__

But alas, I wasn't quite there yet. I figured out the issue I was having and ultimately fixed it, but the auora surrounding this part still puzzles me. (Edit: I figured it out.)

- Re: Step 6 in Praise the Elastic Load Balancer

This tutorial is coming out on a rush deadline so I don't really have time to investigate.

If anyone happens to develop a theory of how security groups for load balancers work, I would love to hear about it!

#### The Problem

When I visited `cs373-backend.canmoo.me`, the request would just time out.

#### The Solution

I used a DNS resolver to confirm `cs373-backend.canmoo.me` mapped to a real IP (the load balancer). And so I figured the requests were getting stuck at the load balancer.

#### STEP 1

I navigated to my load balancer.

<img src="./img/elb-sec-fr-9.png" height="300px"/>

#### STEP 2

I scrolled down to my security group.

<img src="./img/elb-sec-fr-10.png" height="300px"/>

#### STEP 3

I changed the security group over to the same one I used for the EC2 instance and saved those changes.

- This is the part that confuses me because I thought all requests were allowed on the default VPC security group.

- <b>Edit: ACTUALLY I FIGURED IT OUT.</b> The default VPC security group only allows incoming connections from within the VPC itself. So to accept external connections with out the edits in step 4, you would need to make a new secrurity group and attach it to the load balancer.

Requests were still not getting through so I tried editing the inbound rules on the EC2 instance security group next.

<img src="./img/elb-sec-fr-11.png" height="300px"/>

#### STEP 4

I allowed connections on all ports, theorizing that somehow the connections to the load balancer on ports 80 and 443 were getting caught here.

<img src="./img/elb-sec-fr-12.png" height="300px"/>

#### DONE

I sleep now.

<img src="./img/elb-sec-fr-13.png" height="150px"/>


## V. That Which is Left Undone

Like I said, rush deadline. Maybe I'll add on to this for next semester (or maybe YOU would like to!), but if you need help with any of these things please come to my office hours or help session!

- Detaching the Docker container in the EC2 instance

- A version for Elastic Beanstalk

- Creating a database in RDS

- Creating models in SQLAlchemy

- Connecting to an RDS instance using SQLAlchemy

Happy Hosting!

Canyon