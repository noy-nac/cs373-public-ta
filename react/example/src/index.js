import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

// DOM = document object model
// document = HTML code
// object model = object representation of the HTML as a tree of nodes
// manipulate the object, and React will manipulate the HTML
const root = ReactDOM.createRoot(
    // React root need not be the html element with ID root
    document.getElementById('cat')
);

root.render(
  <React.StrictMode>
        <App name="potato" name2="asdsadf" />
        <App name="potato2 potato harder" name2="asdsadf" />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
