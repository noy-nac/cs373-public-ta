import logo from './logo.svg';
import './App.css';

// arguments passed to <App here=val andHere=val2/> are mapped in props
function App(props) {

    return (
        // can do <></> tags in JSX
        <>
            <div className="App" >
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>
                        Edit <code>src/App.js</code> and save to reload.
                    </p>
                    <a
                        // inline style uses a style mapping {key:val}
                        style={{ color: "red", size: 13 }}
                        className="App-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        {/* get access to a variable in App */ props.name}
                    </a>
                </header>
            </div>
        </>

    );
}

export default App;
